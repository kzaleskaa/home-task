<h1 align="center">Random Word Title</h1>

## 📝 Description
   For this task, I am using two external APIs. Random-words-api allows me to collect a user-specified number of unique words. MusicBrainz API allows me to search for information about music, artists and etc. For this project, I used the Python 3.9.6 programming language.

## 🏃‍ How to install and run it?
1. Clone repository
    ```
    git clone https://gitlab.com/kzaleskaa/home-task
    cd home-task
    ```
2. Create your own Python virtual environment.
    ```
    python -m venv venv 
   ```
3. Activate created environment.
    ```
   venv\Scripts\activate 
   ```
4. Install all necessary packages.
    ```
    pip install requirements.txt 
    ```
5. Run app.
    ```
    python main.py
    ```

## 💻 How to use it?
When the application starts and "Enter number (5-20)" is displayed, enter a number from the specified range. If the user enters a number outside the range, the program will display an appropriate message and will terminate the operation. 