from Words import Words
from constants import MIN_RANGE_NUMBER, MAX_RANGE_NUMBER

def main():
    """
    This function reads the data entered by the user. If the number is not
    in the range then no song search will be performed.
    """

    number = input(f"Enter number ({MIN_RANGE_NUMBER}-{MAX_RANGE_NUMBER}):")

    if MIN_RANGE_NUMBER <= int(number) <= MAX_RANGE_NUMBER:
        x = Words(int(number))
        x.run_symulation()
    else:
        print("The number you entered is incorrect. Try again!")

if __name__ == "__main__":
    main()