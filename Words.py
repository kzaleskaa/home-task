import requests
from constants import RANDOM_WORD_API_URL, MUSIC_API_URL

class Words:
    """Class represents generating random words and songs list."""

    def __init__(self, number: int) -> None:
        """
        Constructor of the Words class.

        Args:
             number (int) - The value entered by the user, determines the number of random words to find.
        """

        self.number = number
        self.words = []
        self.songs = []

    def __str__(self) -> None:
        return f"{self.words}"

    def run_symulation(self) -> None:
        """Run functions to create words' list, songs' info and present it."""

        self.create_words_list()
        self.present_words()

        self.get_song_info()
        self.present_songs()

    def present_words(self) -> None:
        """The function presents all collected words."""

        print(f"Random {self.number} words:")

        for index, word in enumerate(self.words):
            print(f"{index + 1}. {word}")

    def present_songs(self) -> None:
        """The function presents all collected songs [artist - album - title]"""

        print(f"Recordings with a random word in title. Word -> artist - album - title")

        for index, song in enumerate(self.songs):
            print(f"{self.words[index]} -> {song}")

    def generate_word(self) -> dict:
        """The function sends get request to receive new random word."""

        response = requests.get(RANDOM_WORD_API_URL)
        response = response.json()[0]

        return response

    def create_words_list(self):
        """The function generates new words and checks if they already exist in the list. """

        while len(self.words) != self.number:
            new_word = self.generate_word()

            if new_word not in self.words:
                self.words.append(new_word["word"])

        self.words = sorted(self.words)

    def generate_songs(self, word) -> str:
        """The function generates information about the songs."""

        response = requests.get(f"{MUSIC_API_URL}{word}&limit=1&fmt=json")

        response = response.json()
        response = response

        recording = response["recordings"]

        if len(recording) == 0:
            return "No recording found!"

        title = recording[0]["title"]
        artist = recording[0]["artist-credit"][0]["name"]
        album = recording[0]["releases"][0]["title"]

        return f"{artist} - {album} - {title} "

    def get_song_info(self):
        """For each word, the function searches for song information."""

        for x in self.words:
            self.songs.append(self.generate_songs(x))
